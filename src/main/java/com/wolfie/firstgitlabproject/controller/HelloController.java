package com.wolfie.firstgitlabproject.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wolfie
 */
@RestController
@RequestMapping("/hello")
public class HelloController {

    @Value("${spring.application.name}")
    private String person;

    /**
     *
     * @return
     */
    @RequestMapping("/from")
    public String from() {
        return "Hello from " + person;
    }

}
