package com.wolfie.firstgitlabproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FirstGitlabProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(FirstGitlabProjectApplication.class, args);
	}

}
